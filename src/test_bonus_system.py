import string
import bonus_system
import random


def test_invalid_program():
    a = random.randint(-10000, 1000000)
    assert round(bonus_system.calculateBonuses("Su", a), 2) == 0
    assert round(bonus_system.calculateBonuses("Sa", a), 2) == 0
    assert round(bonus_system.calculateBonuses("Dz", a), 2) == 0
    assert round(bonus_system.calculateBonuses("Da", a), 2) == 0
    assert round(bonus_system.calculateBonuses("Pa", a), 2) == 0
    assert round(bonus_system.calculateBonuses("Pz", a), 2) == 0


# boundaries
def test_standard_equal_10000():
    a = 10000
    result = bonus_system.calculateBonuses("Standard", a)
    assert round(result, 2) == 0.75


def test_premium_equal_10000():
    a = 10000
    result = bonus_system.calculateBonuses("Premium", a)
    assert round(result, 2) == 0.15


def test_diamond_equal_10000():
    a = 10000
    result = bonus_system.calculateBonuses("Diamond", a)
    assert round(result, 2) == 0.3


def test_standard_equal_50000():
    a = 50000
    result = bonus_system.calculateBonuses("Standard", a)
    assert round(result, 2) == 1


def test_premium_equal_50000():
    a = 50000
    result = bonus_system.calculateBonuses("Premium", a)
    assert round(result, 2) == 0.2


def test_diamond_equal_50000():
    a = 50000
    result = bonus_system.calculateBonuses("Diamond", a)
    assert round(result, 2) == 0.4


def test_standard_equal_100000():
    a = 100000
    result = bonus_system.calculateBonuses("Standard", a)
    assert round(result, 2) == 1.25


def test_premium_equal_100000():
    a = 100000
    result = bonus_system.calculateBonuses("Premium", a)
    assert round(result, 2) == 0.25


def test_diamond_equal_100000():
    a = 100000
    result = bonus_system.calculateBonuses("Diamond", a)
    assert round(result, 2) == 0.5


# 10000 testing
def test_standard_10000():
    a = random.randint(-10000, 9999)
    result = bonus_system.calculateBonuses("Standard", a)
    assert round(result, 2) == 0.5


def test_premium_10000():
    a = random.randint(-10000, 9999)
    result = bonus_system.calculateBonuses("Premium", a)
    assert round(result, 2) == 0.1


def test_diamond_10000():
    a = random.randint(-10000, 9999)
    result = bonus_system.calculateBonuses("Diamond", a)
    assert round(result, 2) == 0.2


# 50000 testing
def test_standard_50000():
    a = random.randint(10000, 49999)
    result = bonus_system.calculateBonuses("Standard", a)
    assert round(result, 2) == 0.75


def test_premium_50000():
    a = random.randint(10000, 49999)
    result = bonus_system.calculateBonuses("Premium", a)
    assert round(result, 2) == 0.15


def test_diamond_50000():
    a = random.randint(10000, 49999)
    result = bonus_system.calculateBonuses("Diamond", a)
    assert round(result, 2) == 0.3


# 100000 testing
def test_standard_100000():
    a = random.randint(50000, 99999)
    result = bonus_system.calculateBonuses("Standard", a)
    assert round(result, 2) == 1


def test_premium_100000():
    a = random.randint(50000, 99999)
    result = bonus_system.calculateBonuses("Premium", a)
    assert round(result, 2) == 0.2


def test_diamond_100000():
    a = random.randint(50000, 99999)
    result = bonus_system.calculateBonuses("Diamond", a)
    assert round(result, 2) == 0.4


def test_standard_more_equal_100000():
    a = random.randint(100000, 1000000)
    result = bonus_system.calculateBonuses("Standard", a)
    assert round(result, 2) == 1.25


def test_premium_more_equal_100000():
    a = random.randint(100000, 1000000)
    result = bonus_system.calculateBonuses("Premium", a)
    assert round(result, 2) == 0.25


def test_diamond_more_equal_100000():
    a = random.randint(100000, 1000000)
    result = bonus_system.calculateBonuses("Diamond", a)
    assert round(result, 2) == 0.5
